package com.example.bootjwt.Dao;

import com.example.bootjwt.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployerJpaRepository extends JpaRepository<Employer, Long> {

}
