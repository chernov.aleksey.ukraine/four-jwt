package com.example.bootjwt.service;

import com.example.bootjwt.domain.User;
import com.example.bootjwt.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Optional<User> getByLogin(@NonNull String login) {

        return userRepository.findUsersByUserName(login);
    }

}