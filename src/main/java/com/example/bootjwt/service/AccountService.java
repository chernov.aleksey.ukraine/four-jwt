package com.example.bootjwt.service;

import com.example.bootjwt.Dao.AccountJpaRepository;
import com.example.bootjwt.model.Account;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Transactional
public class AccountService {
    private final AccountJpaRepository accountJpaRepository;

    public boolean depositAccount(String number, Double amount){
        Account account = accountJpaRepository.findByNumber(number);
        if(account != null){
            return this.depositAccount(account, amount);
        }
        return false;
    }
    public boolean depositAccount(Account account, Double amount){
        account.setBalance(account.getBalance() + amount);
        return true;
    }
    public boolean withdrawalMoney(String number, Double amount){
        Account account = accountJpaRepository.findByNumber(number);
        if(account.getBalance() >= amount){
            return this.withdrawalMoney(account, amount);
        }
        return false;
    }
    public boolean withdrawalMoney(Account account, Double amount){
        if(account.getBalance() >= amount){
            account.setBalance(account.getBalance() - amount);
            return true;
        }
        return false;
    }
    public boolean sendMoney(String numberSender, String numberReceiver, Double amount){
        Account accountSender = accountJpaRepository.findByNumber(numberSender);
        Account accountReceiver = accountJpaRepository.findByNumber(numberReceiver);

        if(!this.withdrawalMoney(accountSender, amount)){
            return false;
        };
        if(!this.depositAccount(accountReceiver, amount)){
            this.depositAccount(accountSender, amount);
            return false;
        };
        return true;
    }
}
