package com.example.bootjwt.model;

import com.example.bootjwt.utilities.Currency;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDtoResponse {

    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private Long userId;
}
