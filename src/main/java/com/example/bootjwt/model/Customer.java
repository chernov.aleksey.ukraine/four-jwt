package com.example.bootjwt.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "customers")
@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(of={"id"})
/*@NamedEntityGraph(name = "customerWithAccountsAndEmployers",
        attributeNodes = {@NamedAttributeNode("accounts"), @NamedAttributeNode(value = "employers")})*/
public class Customer extends AbstractEntity {

    private String name;
    private String email;
    private Integer age;
    private String phone;
    private String password;

    @OneToMany
//            (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @Fetch(value = FetchMode.SUBSELECT)
    @JoinColumn(name = "CUSTOMER_ID")
    private List<Account> accounts = new ArrayList<>();

//    @ManyToMany(fetch = FetchType.EAGER)
//    @Fetch(value = FetchMode.SUBSELECT)
@ManyToMany(cascade = {
        CascadeType.DETACH,
        CascadeType.MERGE,
        CascadeType.REFRESH,
        CascadeType.PERSIST})
    @JoinTable(name = "CUSTOMEREMPLOYMENT",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "employer_id"))
    private Set<Employer> employers = new HashSet<>();
  /*  private String password;*/

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
    public Customer(Long id, String name, String email, Integer age) {
        this.setId(id);
        this.name = name;
        this.email = email;
        this.age = age;
    }
    public Customer(String name, String email, Integer age,String phone,String password) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.phone = phone;
        this.password = password;
    }
/*    @Override
    public String toString() {
        return "Customer{" +
                "id=" + this.getId() +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                ", accounts=" + accounts +
                ", employers=" + employers +
                "}/n";
    }*/
}
