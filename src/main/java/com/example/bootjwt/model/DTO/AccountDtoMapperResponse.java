package com.example.bootjwt.model.DTO;

import com.example.bootjwt.model.*;
import com.example.bootjwt.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperResponse extends DtoMapperFacade<Account, AccountDtoResponse> {
    public AccountDtoMapperResponse() {
        super(Account.class, AccountDtoResponse.class);
    }

    @Override
    protected void decorateDto(AccountDtoResponse dto, Account entity) {
        dto.setId(entity.getId());
        dto.setNumber(entity.getNumber());
        dto.setCurrency(entity.getCurrency());
        dto.setBalance(entity.getBalance());
    }
}
