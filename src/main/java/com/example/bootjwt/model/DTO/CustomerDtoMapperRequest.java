package com.example.bootjwt.model.DTO;

import com.example.bootjwt.model.Customer;
import com.example.bootjwt.model.CustomerDtoRequest;
import com.example.bootjwt.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperRequest extends DtoMapperFacade<Customer, CustomerDtoRequest> {

    public CustomerDtoMapperRequest() {
        super(Customer.class, CustomerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Customer entity, CustomerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAge(dto.getAge());
        entity.setEmail(dto.getEmail());
        entity.setPhone(dto.getPhone());
        entity.setPassword(dto.getPassword());
        entity.setAccounts(dto.getAccounts());
        entity.setEmployers(dto.getEmployers());
    }
}
