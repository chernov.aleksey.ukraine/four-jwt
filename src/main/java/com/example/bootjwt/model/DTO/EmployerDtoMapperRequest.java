package com.example.bootjwt.model.DTO;

import com.example.bootjwt.model.Customer;
import com.example.bootjwt.model.CustomerDtoRequest;
import com.example.bootjwt.model.Employer;
import com.example.bootjwt.model.EmployerDtoRequest;
import com.example.bootjwt.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class EmployerDtoMapperRequest extends DtoMapperFacade<Employer, EmployerDtoRequest> {

    public EmployerDtoMapperRequest() {
        super(Employer.class, EmployerDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Employer entity, EmployerDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
    }
}
