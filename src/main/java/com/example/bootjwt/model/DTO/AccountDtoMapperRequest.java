package com.example.bootjwt.model.DTO;

import com.example.bootjwt.model.Account;
import com.example.bootjwt.model.AccountDtoRequest;
import com.example.bootjwt.model.Customer;
import com.example.bootjwt.model.CustomerDtoRequest;
import com.example.bootjwt.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class AccountDtoMapperRequest extends DtoMapperFacade<Account, AccountDtoRequest> {

    public AccountDtoMapperRequest() {
        super(Account.class, AccountDtoRequest.class);
    }

    @Override
    protected void decorateEntity(Account entity, AccountDtoRequest dto) {
        entity.setId(dto.getId());
        entity.setNumber(dto.getNumber());
        entity.setCurrency(dto.getCurrency());
        entity.setBalance(dto.getBalance());
    }
}
