package com.example.bootjwt.model.DTO;

import com.example.bootjwt.model.Customer;
import com.example.bootjwt.model.CustomerDtoResponse;
import com.example.bootjwt.service.DtoMapperFacade;
import org.springframework.stereotype.Service;

@Service
public class CustomerDtoMapperResponse extends DtoMapperFacade<Customer, CustomerDtoResponse> {
    public CustomerDtoMapperResponse() {
        super(Customer.class, CustomerDtoResponse.class);
    }

    @Override
    protected void decorateDto(CustomerDtoResponse dto, Customer entity) {
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setAge(entity.getAge());
        dto.setEmail(entity.getEmail());
        dto.setAccounts(entity.getAccounts());
        dto.setEmployers(entity.getEmployers());
    }
}
