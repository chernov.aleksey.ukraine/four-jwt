package com.example.bootjwt.model;

import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDtoRequest {

    @NotNull
    private Long id;

    @NotNull
    @Size(min = 2, message = "user name should have at least 2 characters")
    private String name;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Min(18)
    private Integer age;

    @NotBlank
    @Pattern(regexp = "(\\+38|0)[0-9]{9}")
    private String phone;

    @NotBlank
    private String password;
    private List<Account> accounts;
    private Set<Employer> employers;

}
