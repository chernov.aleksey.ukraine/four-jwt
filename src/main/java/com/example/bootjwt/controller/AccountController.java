package com.example.bootjwt.controller;

import com.example.bootjwt.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountService accountService;


    @PostMapping("/deposit")
    public ResponseEntity<?> depositMoney(String accountNumber, Double amount){
        return accountService.depositAccount(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Depositing of the account is failed");
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawalMoney(String accountNumber, Double amount){
        return accountService.withdrawalMoney(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Withdrawing from the account is failed");
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendMoney( String accountNumberSender, String accountNumberReceiver, Double amount){
        return accountService.sendMoney(accountNumberSender, accountNumberReceiver, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Sending money is failed");
    }
}
