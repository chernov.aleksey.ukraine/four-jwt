package com.example.bootjwt.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.bootjwt.model.CustomerDtoRequest;
import com.example.bootjwt.model.CustomerDtoResponse;
import com.example.bootjwt.model.DTO.CustomerDtoMapperRequest;
import com.example.bootjwt.model.DTO.CustomerDtoMapperResponse;
import com.example.bootjwt.utilities.Currency;
import com.example.bootjwt.service.CustomerService;
import com.example.bootjwt.model.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerDtoMapperResponse customerDtoMapperResponse;
    private final CustomerDtoMapperRequest customerDtoMapperRequest;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(customerService.getAllCustomers().stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> getAllUsers(@PathVariable Integer page, Integer size) {
        return ResponseEntity.ok(customerService.getAllCustomers(page, size).stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.getCustomerFullInfo(id)));
    }
    @PostMapping("/")
    public CustomerDtoResponse postNewUser(String name, String email, Integer age,String phone,String password ){
        Customer customer = new Customer(name, email, age, phone, password);
        return customerDtoMapperResponse.convertToDto(customerService.createCustomer(customer));
    }
    @PutMapping("/")
    public CustomerDtoResponse updateUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.updateCustomer(customer));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(Long customerId, String currency){
         if(customerService.getCustomerFullInfo(customerId) == null) {ResponseEntity.badRequest().body("Customer not found");}
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.openAccount(customerService.getCustomerFullInfo(customerId), Currency.forValue(currency))));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(Long customerId, Long accountId) {
//        ObjectMapper mapper = new ObjectMapper();
//        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(customerId);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.deleteAccount(accountId)));
    }
}
